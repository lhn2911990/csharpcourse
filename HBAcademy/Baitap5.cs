﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HBAcademy
{
    internal class Baitap_5
    {
        public static void P5()
        {
            Console.Write("nhập số a: ");
            int a = int.Parse(Console.ReadLine());
            Console.Write("nhập số b: ");
            int b = int.Parse(Console.ReadLine());
            int sum = a + b;
            int minus = a - b;
            int multiply = a * b;
            float divide = a / b;
            Console.WriteLine("a + b = :" + sum);
            Console.WriteLine("a - b = :" + minus);
            Console.WriteLine("a * b = :" + multiply);
            Console.WriteLine("a / b = :" + divide);
        }

        public static void P6()
        {
            Console.Write("nhập số a: ");
            int a = int.Parse(Console.ReadLine());
            Console.Write("nhập số b: ");
            int b = int.Parse(Console.ReadLine());

            if (a > b)
            {
                Console.WriteLine("a > b");
            }
            if (a == b)
            {
                Console.WriteLine("a = b");
            }
            if (a < b)
            {
                Console.WriteLine("a < b");
            }
        }
        public static void P7()
        {
            // Khai báo và khởi tạo hai biến số bị chia và số chia
            Console.Write("nhập số a: ");
            int soBiChia = int.Parse(Console.ReadLine());
            Console.Write("nhập số b: ");
            int soChia = int.Parse(Console.ReadLine());
            // Hiển thị phép tính là phép chia giữa hai số
            Console.WriteLine("Tinh: {0} / {1} = ?", soBiChia, soChia); 
            int phanDu = soBiChia % soChia;
            int phanNguyen = soBiChia / soChia;
            Console.WriteLine("Phần nguyên của phep chia là: " + phanNguyen);
            Console.WriteLine("Phần Dư của phep chia là: " + phanDu);
        }
    }
}
