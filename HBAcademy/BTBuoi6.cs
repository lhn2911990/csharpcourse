﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HBAcademy
{
    internal class BTBuoi6
    {
        public static void EX1()
        {
            // Khai báo mảng 10 phần tử
            int[] taphopso = new int[10];

            // Nhập giá trị cho các phần tử trong mảng
            Console.WriteLine("Nhập giá trị cho 10 phần tử trong mảng: ");
            for (int i = 0; i < 10; i++)
            {
                Console.Write("Phần tử thứ " + (i + 1) + ": ");
                taphopso[i] = int.Parse(Console.ReadLine());
            }

            // In các phần tử ra màn hình theo thứ tự index
            Console.WriteLine("Các phần tử theo thứ tự index: ");
            for (int i = 0; i < 10; i++)
            {
                Console.Write(taphopso[i] + " ");
            }
            Console.WriteLine();

            // In các phần tử ra màn hình theo thứ tự ngược lại
            Console.WriteLine("Các phần tử theo thứ tự ngược lại: ");
            for (int i = 9; i >= 0; i--)
            {
                Console.Write(taphopso[i] + " ");
            }
            Console.WriteLine();
            // Tính tổng các phần tử
            int sum = 0, sumOdd = 0, sumEven = 0, sumPositive = 0;
            for (int i = 0; i < 10; i++)
            {
                sum += taphopso[i];
                if (taphopso[i] % 2 == 0)
                {
                    sumEven += taphopso[i];
                }
                else
                {
                    sumOdd += taphopso[i];
                }
                if (taphopso[i] > 0)
                {
                    sumPositive += taphopso[i];
                }
            }

            // In kết quả
            Console.WriteLine("Tổng các phần tử: " + sum);
            Console.WriteLine("Tổng các phần tử lẻ: " + sumOdd);
            Console.WriteLine("Tổng các phần tử chẵn: " + sumEven);
            Console.WriteLine("Tổng các phần tử dương: " + sumPositive);
        }
        public static void EX2()
        {
            // Khai báo mảng 2 chiều 3x3
            int[,] array = new int[3, 3];

            // Đọc giá trị từ bàn phím và gán cho từng phần tử trong mảng
            Console.WriteLine("Nhập giá trị cho từng phần tử trong mảng:");
            for (int i = 0; i < array.GetLength(0); i++) // Lặp qua từng hàng
            {
                for (int j = 0; j < array.GetLength(1); j++) // Lặp qua từng cột
                {
                    bool isValidInput = false;
                    while (!isValidInput)
                    {
                        Console.Write("Phần tử ở hàng " + (i + 1) + ", cột " + (j + 1) + ": ");
                        string input = Console.ReadLine();
                        if (int.TryParse(input, out int value))
                        {
                            array[i, j] = value;
                            isValidInput = true;
                        }
                        else
                        {
                            Console.WriteLine("Giá trị nhập không hợp lệ. Vui lòng nhập lại.");
                        }
                        Console.Write("Phần tử ở hàng " + (i + 1) + ", cột " + (j + 1) + ": ");
                    }
                    array[i, j] = int.Parse(Console.ReadLine());
                }
            }

            // In ra màn hình giá trị của tất cả các phần tử trong mảng
            Console.WriteLine("Giá trị của các phần tử trong mảng:");
            for (int i = 0; i < array.GetLength(0); i++) // Lặp qua từng hàng
            {
                for (int j = 0; j < array.GetLength(1); j++) // Lặp qua từng cột
                {
                    Console.Write(array[i, j] + " ");
                }
                Console.WriteLine(); // Xuống dòng sau khi lặp qua một hàng
            }
        }
    }
}
