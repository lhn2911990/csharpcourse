﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace HBAcademy
{
    internal class BTVNBuoi5
    {
        public static void BTSlide18()
        {
            int a, b; // Khai báo tham số

            // Nhập số nguyên a với kiểm tra tính hợp lệ
            while (true)  // lặp lại cho đến khi nào input giá trị hợp lệ thì thôi
            {
                Console.Write("Nhập số nguyên a: ");
                if (int.TryParse(Console.ReadLine(), out a))
                {
                    break; // nếu đúng thì sẽ thoát vòng lặp và chạy khối lệnh tiếp theo
                }
                else
                {
                    Console.WriteLine("Giá trị nhập vào không hợp lệ. Vui lòng nhập lại.");
                }
            }

            // Nhập số nguyên b với kiểm tra tính hợp lệ
            while (true)   // lặp lại cho đến khi nào input giá trị hợp lệ thì thôi
            {
                Console.Write("Nhập số nguyên b: ");
                if (int.TryParse(Console.ReadLine(), out b))
                {
                    break;    // nếu đúng thì sẽ thoát vòng lặp và chạy khối lệnh tiếp theo
                }
                else
                {
                    Console.WriteLine("Giá trị nhập vào không hợp lệ. Vui lòng nhập lại.");
                }
            }

            // Kiểm tra điều kiện và in ra kết quả tương ứng
            if (a > 0)  // nếu a > 0 chạy khối lệnh bên dưới đây
            {
                if (b > 0)
                {
                    Console.WriteLine($"Hai số a và b: {a}, {b}");
                }
                else
                {
                    Console.WriteLine($"Số A: {a}");
                }
            }
            else  // a khác 0 thì chạy khối lệnh dưới đây
            {
                if (b > 0)
                {
                    Console.WriteLine($"Số B: {b}");
                }
                else
                {
                    Console.WriteLine("Không có số nào lớn hơn 0");
                }
            }
        }
        public static void BTSlide20()
        {
            int a, b;
            char d;

            // Nhập số nguyên a với kiểm tra tính hợp lệ
            while (true)   // lặp lại cho đến khi nào input giá trị hợp lệ thì thôi
            {
                Console.Write("Nhập số nguyên a: ");
                if (int.TryParse(Console.ReadLine(), out a))
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Giá trị nhập vào không hợp lệ. Vui lòng nhập lại.");
                }
            }

            // Nhập số nguyên b với kiểm tra tính hợp lệ
            while (true)   // lặp lại cho đến khi nào input giá trị hợp lệ thì thôi
            {
                Console.Write("Nhập số nguyên b: ");
                if (int.TryParse(Console.ReadLine(), out b))
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Giá trị nhập vào không hợp lệ. Vui lòng nhập lại.");
                }
            }

            // Nhập dấu toán tử d với kiểm tra tính hợp lệ
            while (true) // lặp lại cho đến khi nào input giá trị hợp lệ thì thôi
            {
                Console.Write("Nhập dấu toán tử (+, -, *, /): ");
                string input = Console.ReadLine();
                if (input.Length == 1 && "+-*/".Contains(input)) // nếu input có độ dài là 1 ký tự , và chứa các ký tự toán tử hợp lệ 
                {
                    d = input[0]; // Truy cập ký tự tại vị trí đầu tiên của chuỗi. 
                    break; // thoát vòng lặp nếu đã thỏa mãn điều kiện ký tự hợp lệ
                }
                else   // Yêu cầu nhập lại nếu chưa hợp lệ
                {
                    Console.WriteLine("Dấu toán tử không hợp lệ. Vui lòng nhập lại.");
                }
            }

            // Thực hiện phép tính và in ra kết quả

            double result = 0;
            bool bieuthuchople = true;

            switch (d)
            {
                case '+':
                    result = a + b;
                    break;
                case '-':
                    result = a - b;
                    break;
                case '*':
                    result = a * b;
                    break;
                case '/':
                    if (b != 0)
                    {
                        result = (double)a / b;
                    }
                    else  // phép chia cho 0 là không hợp lệ và báo lỗi
                    {
                        Console.WriteLine("Lỗi: Không thể chia cho 0.");
                        bieuthuchople = false;
                    }
                    break;
                default:
                    bieuthuchople = false;
                    break;
            }

            if (bieuthuchople) // trả kết quả
            {
                Console.WriteLine($"Kết quả của {a} {d} {b} là: {result}");
            }
        }
        public static void BTSlide27()
        {
            int N;

            // Nhập số nguyên dương N với kiểm tra tính hợp lệ
            while (true)
            {
                Console.Write("Nhập số nguyên dương N: ");
                if (int.TryParse(Console.ReadLine(), out N) && N > 0)
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Giá trị nhập vào không hợp lệ. Vui lòng nhập lại.");
                }
            }

            // Tính tổng từ 1 đến N sử dụng vòng lặp for
            int sumFor = 0;
            for (int i = 1; i <= N; i++)  //Vòng lặp for bắt đầu từ i = 1 đến i <= N, và mỗi lần lặp, sumFor sẽ được cộng thêm i.
            {
                sumFor += i;
            }
            Console.WriteLine("Tổng từ 1 đến " + N + " (sử dụng for): " + sumFor);

            // Tính tổng từ 1 đến N sử dụng vòng lặp while
            int sumWhile = 0; //Khởi tạo sumWhile là 0.
            int j = 1;          //Khởi tạo biến j là 1.
            while (j <= N)  //Vòng lặp while tiếp tục chạy khi j <= N.  và 
            {
                sumWhile += j; //Mỗi lần lặp, sumWhile được cộng thêm j
                j++;            //j được tăng thêm 1.
            }
            Console.WriteLine("Tổng từ 1 đến " + N + " (sử dụng while): " + sumWhile);

            // Tính tổng từ 1 đến N sử dụng vòng lặp do-while
            int sumDoWhile = 0; //Khởi tạo sumDoWhile là 0.
            int k = 1;          //Khởi tạo biến k là 1.

            do    //Vòng lặp do-while sẽ luôn thực hiện ít nhất một lần
            {
                sumDoWhile += k;   //Trong mỗi lần lặp, sumDoWhile được cộng thêm k
                k++;                //k được tăng thêm 1. 
            } while (k <= N);       //Vòng lặp tiếp tục cho đến khi k > N
            Console.WriteLine("Tổng từ 1 đến " + N + " (sử dụng do-while): " + sumDoWhile);
        }
        public static void EX2()
        {
            int N;

            // Nhập số nguyên N với kiểm tra tính hợp lệ
            while (true)
            {
                Console.Write("Nhập số lượng số nguyên N: ");
                if (int.TryParse(Console.ReadLine(), out N) && N > 0)
                {
                    break; //Pass
                }
                else
                {
                    Console.WriteLine("Giá trị nhập vào không hợp lệ. Vui lòng nhập lại.");
                }
            }

            // Khởi tạo biến để lưu số nhỏ nhất
            int min = int.MaxValue; //đảm bảo bất kỳ số nguyên nào nhập vào đều sẽ nhỏ hơn giá trị này.

            // Nhập N số nguyên và tìm số nhỏ nhất
            for (int i = 1; i <= N; i++)
            {
                int currentNumber;
                while (true)
                {
                    Console.Write($"Nhập số nguyên thứ {i}: ");
                    if (int.TryParse(Console.ReadLine(), out currentNumber))
                    {
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Giá trị nhập vào không hợp lệ. Vui lòng nhập lại.");
                    }
                }

                // Cập nhật số nhỏ nhất
                if (currentNumber < min) //Nếu currentNumber nhỏ hơn min, thì min được cập nhật với giá trị mới của currentNumber.
                {
                    min = currentNumber;
                }
            }

            // In ra số nhỏ nhất
            Console.WriteLine("Số nhỏ nhất trong các số đã nhập là: " + min);
        }
        public static void EX3()
        {
            int N;

            // Nhập số nguyên N với kiểm tra tính hợp lệ
            while (true)
            {
                Console.Write("Nhập số lượng số nguyên N: ");
                if (int.TryParse(Console.ReadLine(), out N) && N > 0)
                {
                    break; //Pass
                }
                else
                {
                    Console.WriteLine("Giá trị nhập vào không hợp lệ. Vui lòng nhập lại.");
                }
            }

            // Khởi tạo biến để lưu số lớn nhất
            int max = int.MinValue; //đảm bảo bất kỳ số nguyên nào nhập vào đều sẽ lớn hơn giá trị này.

            // Nhập N số nguyên và tìm số lớn nhất
            for (int i = 1; i <= N; i++)
            {
                int currentNumber;
                while (true)
                {
                    Console.Write($"Nhập số nguyên thứ {i}: ");
                    if (int.TryParse(Console.ReadLine(), out currentNumber))
                    {
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Giá trị nhập vào không hợp lệ. Vui lòng nhập lại.");
                    }
                }

                // Cập nhật số lớn nhất
                if (currentNumber > max) //Nếu currentNumber lớn hơn max, thì max được cập nhật với giá trị mới của currentNumber.
                {
                    max = currentNumber;
                }
            }

            // In ra số lớn nhất
            Console.WriteLine("Số lớn nhất trong các số đã nhập là: " + max);
        }
        public static void EX4()
        {
            int N;

            // Nhập số lượng N với kiểm tra tính hợp lệ
            while (true)
            {
                Console.Write("Nhập số lượng số nguyên N: ");
                if (int.TryParse(Console.ReadLine(), out N) && N > 0)
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Giá trị nhập vào không hợp lệ. Vui lòng nhập lại.");
                }
            }

            // Khởi tạo biến để lưu tổng số chẵn
            int sumEven = 0;

            // Nhập N số nguyên và tính tổng các số chẵn
            for (int i = 1; i <= N; i++)
            {
                int currentNumber;
                while (true)
                {
                    Console.Write($"Nhập số nguyên thứ {i}: ");
                    if (int.TryParse(Console.ReadLine(), out currentNumber))
                    {
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Giá trị nhập vào không hợp lệ. Vui lòng nhập lại.");
                    }
                }

                // Kiểm tra nếu số đó là số chẵn thì cộng vào tổng số chẵn
                if (currentNumber % 2 == 0)  // nếu không dư thì là chẵn
                {
                    sumEven += currentNumber;
                }
            }

            // In ra tổng các số chẵn
            Console.WriteLine("Tổng các số chẵn trong các số đã nhập là: " + sumEven);
        }
        public static void EX5()
        {
            int N;

            // Nhập số lượng N với kiểm tra tính hợp lệ
            while (true)
            {
                Console.Write("Nhập số lượng số nguyên N: ");
                if (int.TryParse(Console.ReadLine(), out N) && N > 0)
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Giá trị nhập vào không hợp lệ. Vui lòng nhập lại.");
                }
            }

            // Khởi tạo biến để lưu tổng số lẻ
            int sumOdd = 0;

            // Nhập N số nguyên và tính tổng các số lẻ
            for (int i = 1; i <= N; i++)
            {
                int currentNumber;
                while (true)
                {
                    Console.Write($"Nhập số nguyên thứ {i}: ");
                    if (int.TryParse(Console.ReadLine(), out currentNumber))
                    {
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Giá trị nhập vào không hợp lệ. Vui lòng nhập lại.");
                    }
                }

                // Kiểm tra nếu số đó là số lẻ thì cộng vào tổng số lẻ
                if (currentNumber % 2 != 0)  // nếu dư thì là lẻ
                {
                    sumOdd += currentNumber;
                }
            }

            // In ra tổng các số lẻ
            Console.WriteLine("Tổng các số lẻ trong các số đã nhập là: " + sumOdd);
        }
    }
}