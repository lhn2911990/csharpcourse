﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace HBAcademy
{
    internal class Baitap4
    {
        public static void P4() 
        {

            Console.Write("Nhập số thập phân: ");
            int decimalNumber = Convert.ToInt32(Console.ReadLine());
            string binaryNumber = Convert.ToString(decimalNumber, 2).PadLeft(8, '0');
            Console.WriteLine("Số nhị phân tương ứng (8 bit): " + binaryNumber);

           
            Console.Write("Nhập số nhị phân (8 bit): ");
            string binaryInput = Console.ReadLine();

            // Hàm kiểm tra xem dữ liệu nhập có phải là dữ liệu hợp lệ không.
            static bool IsBinary(string binaryInput)
            {
                return binaryInput.All(c => c == '0' || c == '1');
            }

            if (IsBinary(binaryInput))
            {
                int decimalOutput = Convert.ToInt32(binaryInput, 2);
                Console.WriteLine("Số thập phân tương ứng: " + decimalOutput);
            }
            else
            {
                Console.WriteLine("Số nhị phân nhập vào không hợp lệ.");
            }
        }
    }
}
