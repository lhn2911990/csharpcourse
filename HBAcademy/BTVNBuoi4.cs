﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HBAcademy
{
    internal class BTVNBuoi4
    {
        public static void Ex5()
        {
            Console.Write("nhập số giá trị A: ");
            int a = int.Parse(Console.ReadLine());
            Console.Write("nhập số giá trị B: ");
            int b = int.Parse(Console.ReadLine());
            
            int temp = a; // Biến trung gian

            //In giá trị trước khi tráo
            Console.WriteLine("Trước khi tráo đổi");
            Console.WriteLine("a =" + a);
            Console.WriteLine("b =" + b);

            // Tráo đổi giá trị của 2 biến
            a = b;
            b = temp;
            // in giá trị sau khi đã tráo đổi
            Console.WriteLine("Sau khi tráo đổi");
            Console.WriteLine("a =" + a);
            Console.WriteLine("b =" + b);
        }

        public static void Ex6()
        {
            // Nhập một số từ input stream Console.ReadLine
            Console.Write("Nhập một số nguyên: ");
            string input = Console.ReadLine();

            // Kiểm tra input nhập vào có phải là số nguyên hay không.
            if (int.TryParse(input, out int number))
            {
                // In bảng nhân tới 10 của số đó
                Console.WriteLine($"Bảng nhân của {number}:");
                for (int i = 1; i <= 10; i++)
                {
                    Console.WriteLine($"{number} x {i} = {number * i}");
                }
            }
            else
            {
                // Thông báo lỗi nếu input không phải là số nguyên
                Console.WriteLine("Vui lòng nhập một số nguyên hợp lệ.");
            }
        }

        public static void Ex7()
        {
            //Khai báo biến của 4 số
            Console.Write("nhập số giá trị A: ");
            int Value_a = int.Parse(Console.ReadLine());
            Console.Write("nhập số giá trị B: ");
            int Value_b = int.Parse(Console.ReadLine());
            Console.Write("nhập số giá trị C: ");
            int Value_c = int.Parse(Console.ReadLine());
            Console.Write("nhập số giá trị D: ");
            int Value_d = int.Parse(Console.ReadLine());
            //Tính Tổng của 4 số
            int sum = Value_a + Value_b + Value_c + Value_d;
            //Chia cho 4
            float result = sum / 4;
            //In kết quả ra console
            Console.WriteLine($"Giá trị trung bình của 4 số là: {result}");
        }

        public static void Ex8()
        {
            // Nhập nhiệt độ C
            Console.Write("Nhập nhiệt độ trong độ Celsius: ");
            float Do_C = float.Parse(Console.ReadLine());

            // Chuyển đổi từ Độ C sang Độ K
            float Do_K = Convertto_K(Do_C);

            // Chuyển đổi từ Độ C sang Độ F
            float Do_F = Convertto_F(Do_C);

            //Hàm chuyển đổi từ độ C => Độ K      Công thức K = C+273.15
            static float Convertto_K(float Do_C)
            {
                return Do_C + 273.15f;
            }
            //Hàm chuyển đổi từ độ C => Độ F      Công thức F = (C * 9/5) + 32
            static float Convertto_F(float Do_C)
            {
                return (Do_C * 9/5) + 32;
            }
            // In kết quả
            Console.WriteLine($"Nhiệt độ trong độ Kelvin: {Do_K}");
            Console.WriteLine($"Nhiệt độ trong độ Fahrenheit: {Do_F}");
        }
    }
}