﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace HBAcademy
{
    internal class Bai_1
    {
      public static void Ex1() 
        {     
            //Nhập 1 số nguyên và trả về một ký tự theo bảng ASCII
            Console.Write("Nhập số nguyên: "); // Yêu cầu input của user
            int number = int.Parse(Console.ReadLine()); // đọc từ input của user
            char character = (char)number;
            Console.WriteLine("Ký tự tương ứng : " +  character);
            
            //Nhập 1 ký tự trả về 1 số nguyên ( Theo bảng ASCII)
            Console.Write("Nhập 1 ký tự: "); // Yêu cầu input của user
            char inputchar = Convert.ToChar(Console.ReadLine()); //  đọc từ input của user
            int asciivalue = (int)inputchar;            
            Console.WriteLine("Giá trị ASCII tương ứng: " + asciivalue);

            // Ghi Dòng HBAcademy vào file Welcome.txt
            File.WriteAllText("Welcome.txt", "HBAcademy\n");

            // Thêm Dòng Lập Trình game căn bản
            File.AppendAllText("Welcome.txt", "Lập trình game căn bản\n");

            // Đọc và in ra màn hình từ file welcome.txt
            string content = File.ReadAllText("Welcome.txt");
            Console.WriteLine("Nội dung file welcome.txt:\n" +  content);

        }
    }
}
