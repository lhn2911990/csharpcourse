﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HBAcademy
{
    public class BTVNBuoi6_Slide13
    {
        public static void BTSlide13()
        {
            // Nhập kích thước của mảng 
            int value_n = GetValidInteger("Nhập kích thước của mảng:");
            int[] arr = new int[value_n];

            // Nhập giá trị cho các phần tử
            Console.WriteLine("Nhập {0} giá trị cho mảng:", value_n );
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = GetValidInteger($"Phần tử thứ {i}:");

            }

            // In các phần tử theo thứ tự index
            Console.WriteLine("Mảng theo thứ tự index:");
            PrintArray(arr);

            // In các phần tử theo thứ tự ngược lại
            Console.WriteLine("Mảng theo thứ tự ngược lại:");
            PrintArrayReverse(arr);

            // Tính tổng các phần tử
            int totalSum = arr.Sum();
            Console.WriteLine("Tổng các phần tử: " + totalSum);

            // Tính tổng các phần tử lẻ
            int oddSum = arr.Where(x => x % 2 != 0).Sum();
            Console.WriteLine("Tổng các phần tử lẻ: " + oddSum);

            // Tính tổng các phần tử chẵn
            int evenSum = arr.Where(x => x % 2 == 0).Sum();
            Console.WriteLine("Tổng các phần tử chẵn: " + evenSum);

            // Tính tổng các phần tử dương
            int positiveSum = arr.Where(x => x > 0).Sum();
            Console.WriteLine("Tổng các phần tử dương: " + positiveSum);

            // Đếm số lần xuất hiện của từng phần tử dương
            Console.WriteLine("Số lần xuất hiện của từng phần tử dương:");
            CountOccurrences(arr);

            // Đếm số phần tử âm
            int negativeCount = arr.Count(x => x < 0);
            Console.WriteLine("Số phần tử âm: " + negativeCount);

            // Tìm phần tử lớn nhất
            int max = arr.Max();
            Console.WriteLine("Phần tử lớn nhất: " + max);

            // Tìm phần tử nhỏ nhất
            int min = arr.Min();
            Console.WriteLine("Phần tử nhỏ nhất: " + min);

            // Tìm phần tử lớn thứ n
            while (true)
            {             
                try
                {
                    int n = GetValidInteger("Nhập giá trị n để tìm phần tử lớn thứ n: ");
                    if (n >= 1 && n <= arr.Length)
                    {
                        break; // Thoát khỏi vòng lặp nếu n hợp lệ
                    }
                    int nthLargest = FindNthLargest(arr, n);
                    Console.WriteLine("Phần tử lớn thứ {0}: {1}", n, nthLargest);
                }
                catch (ArgumentOutOfRangeException ex)
                {
                    Console.WriteLine(ex.Message);
                }              
            } 
            // Tìm phần tử nhỏ thứ n
                
            while (true)
            {
                try 
                {
                    int n = GetValidInteger("Nhập giá trị n để tìm phần tử nhỏ thứ {n}: ");
                    if (n >= 1 && n <= arr.Length)
                    {
                        break; // Thoát khỏi vòng lặp nếu n hợp lệ
                    }
                    int nthSmallest = FindNthSmallest(arr, n);
                    Console.WriteLine("Phần tử nhỏ thứ {0}: {1}", n, nthSmallest);
                }
                catch (ArgumentOutOfRangeException ex)
                {
                    Console.WriteLine(ex.Message);
                }              
            }
            
            // Tìm vị trí đầu tiên của một giá trị phần tử
            int value = GetValidInteger("Nhập giá trị để tìm vị trí đầu tiên: ");
            int firstOccurrence = Array.IndexOf(arr, value);
            if (firstOccurrence != -1)
            {
                Console.WriteLine("Giá trị {0} xuất hiện đầu tiên ở vị trí: {1}", value, firstOccurrence);
            }
            else
            {
                Console.WriteLine("Giá trị {0} không tồn tại trong mảng.", value);
            }

            // Sao chép mảng này sang mảng khác
            int[] copiedArr = new int[arr.Length];  // khởi tạo mảng copyarray mới với cùng kích thước với mảng arr.
            Array.Copy(arr, copiedArr, arr.Length);
            Console.WriteLine("Mảng sao chép:");
            PrintArray(copiedArr);

            // Chia mảng thành 2 mảng chẵn lẻ
            int[] even = arr.Where(x => x % 2 == 0).ToArray();
            int[] odd = arr.Where(x => x % 2 != 0).ToArray();
            Console.WriteLine("Mảng chẵn:");
            PrintArray(even);
            Console.WriteLine("Mảng lẻ:");
            PrintArray(odd);

            // Sắp xếp mảng tăng dần
            Array.Sort(arr);
            Console.WriteLine("Mảng sắp xếp tăng dần:");
            PrintArray(arr);

            // Sắp xếp mảng giảm dần
            Array.Sort(arr, (a, b) => b.CompareTo(a));
            Console.WriteLine("Mảng sắp xếp giảm dần:");
            PrintArray(arr);
        }
        static int GetValidInteger(string prompt) // Hàm kiểm tra xem người dùng có nhập đúng dữ liệu không
        {
            int result;
            while (true)
            {
                Console.Write(prompt);
                string input = Console.ReadLine();
                if (int.TryParse(input, out result))
                {
                    return result;
                }
                Console.WriteLine("Giá trị nhập vào không hợp lệ. Vui lòng nhập lại.");
            }
        }
        static void PrintArray(int[] arr)
        {
            foreach (var item in arr)
            {
                Console.Write(item + " ");
            }
            Console.WriteLine();
        }

        static void PrintArrayReverse(int[] arr)
        {
            for (int i = arr.Length - 1; i >= 0; i--)
            {
                Console.Write(arr[i] + " ");
            }
            Console.WriteLine();
        }

        static void CountOccurrences(int[] arr)
        {
            var positiveElements = arr.Where(x => x > 0).GroupBy(x => x);
            foreach (var group in positiveElements)
            {
                Console.WriteLine("Phần tử {0} xuất hiện {1} lần", group.Key, group.Count());
            }
        }

        static int FindNthLargest(int[] arr, int n)
        {
            if (n < 1 || n > arr.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(n), $"Giá trị {n} không hợp lệ. {n} phải nằm trong khoảng từ 1 đến {arr.Length}.");
            }
            var sortedArr = arr.OrderByDescending(x => x).ToArray();
            return sortedArr[n - 1];
        }

        static int FindNthSmallest(int[] arr, int n)
        {
            if (n < 1 || n > arr.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(n), $"Giá trị {n} không hợp lệ. {n} phải nằm trong khoảng từ 1 đến {arr.Length}.");
            }
            var sortedArr = arr.OrderBy(x => x).ToArray();
            return sortedArr[n - 1];
        }
    }

    public class BTVNBuoi6_Slide17()
    {
        public static void Slide17EX()
        {
            // Khai báo hai mảng 2 chiều 3x3
            int[,] matrix1 = new int[3, 3];
            int[,] matrix2 = new int[3, 3];

            // Đọc giá trị từ bàn phím cho mảng thứ nhất
            Console.WriteLine("Nhập các phần tử cho mảng thứ nhất:");
            InputMatrix(matrix1);

            // Đọc giá trị từ bàn phím cho mảng thứ hai
            Console.WriteLine("Nhập các phần tử cho mảng thứ hai:");
            InputMatrix(matrix2);

            // In giá trị tất cả các phần tử của mảng thứ nhất
            Console.WriteLine("Các phần tử của mảng thứ nhất:");
            PrintMatrix(matrix1);

            // In giá trị tất cả các phần tử của mảng thứ hai
            Console.WriteLine("Các phần tử của mảng thứ hai:");
            PrintMatrix(matrix2);

            // Tính tổng các phần tử trên đường chéo chính của mảng thứ nhất
            int mainDiagonalSum = SumMainDiagonal(matrix1);
            Console.WriteLine("Tổng các phần tử trên đường chéo chính của mảng thứ nhất: " + mainDiagonalSum);

            // Tính tổng các phần tử trên đường chéo phụ của mảng thứ nhất
            int secondaryDiagonalSum = SumSecondaryDiagonal(matrix1);
            Console.WriteLine("Tổng các phần tử trên đường chéo phụ của mảng thứ nhất: " + secondaryDiagonalSum);

            // Tính tổng các phần tử của từng hàng của mảng thứ nhất
            Console.WriteLine("Tổng các phần tử của từng hàng của mảng thứ nhất:");
            SumRows(matrix1);

            // Tính tổng các phần tử của từng cột của mảng thứ nhất
            Console.WriteLine("Tổng các phần tử của từng cột của mảng thứ nhất:");
            SumColumns(matrix1);

            // Cộng hai mảng
            Console.WriteLine("Mảng tổng của hai mảng:");
            int[,] sumMatrix = AddMatrices(matrix1, matrix2);
            PrintMatrix(sumMatrix);

            // Trừ hai mảng
            Console.WriteLine("Mảng hiệu của hai mảng:");
            int[,] diffMatrix = SubtractMatrices(matrix1, matrix2);
            PrintMatrix(diffMatrix);

            // Nhân hai mảng
            Console.WriteLine("Mảng tích của hai mảng:");
            int[,] productMatrix = MultiplyMatrices(matrix1, matrix2);
            PrintMatrix(productMatrix);

            // Ma trận chuyển vị của mảng thứ nhất
            Console.WriteLine("Ma trận chuyển vị của mảng thứ nhất:");
            int[,] transposeMatrix = TransposeMatrix(matrix1);
            PrintMatrix(transposeMatrix);

            // Ma trận nghịch đảo của mảng thứ nhất (nếu có thể)
            double[,] inverseMatrix = InverseMatrix(matrix1);
            if (inverseMatrix != null)
            {
                Console.WriteLine("Ma trận nghịch đảo của mảng thứ nhất:");
                PrintMatrix(inverseMatrix);
            }
            else
            {
                Console.WriteLine("Mảng thứ nhất không có ma trận nghịch đảo.");
            }
        }

        static void InputMatrix(int[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    Console.Write("Phần tử [{0},{1}]: ", i, j);
                    matrix[i, j] = int.Parse(Console.ReadLine());
                }
            }
        }

        static void PrintMatrix(int[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    Console.Write(matrix[i, j] + "\t");
                }
                Console.WriteLine();
            }
        }

        static void PrintMatrix(double[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    Console.Write(matrix[i, j] + "\t");
                }
                Console.WriteLine();
            }
        }

        static int SumMainDiagonal(int[,] matrix)
        {
            int sum = 0;
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                sum += matrix[i, i];
            }
            return sum;
        }

        static int SumSecondaryDiagonal(int[,] matrix)
        {
            int sum = 0;
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                sum += matrix[i, matrix.GetLength(1) - 1 - i];
            }
            return sum;
        }

        static void SumRows(int[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                int sum = 0;
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    sum += matrix[i, j];
                }
                Console.WriteLine("Tổng các phần tử hàng {0}: {1}", i, sum);
            }
        }

        static void SumColumns(int[,] matrix)
        {
            for (int j = 0; j < matrix.GetLength(1); j++)
            {
                int sum = 0;
                for (int i = 0; i < matrix.GetLength(0); i++)
                {
                    sum += matrix[i, j];
                }
                Console.WriteLine("Tổng các phần tử cột {0}: {1}", j, sum);
            }
        }

        static int[,] AddMatrices(int[,] matrix1, int[,] matrix2)
        {
            int[,] result = new int[matrix1.GetLength(0), matrix1.GetLength(1)];
            for (int i = 0; i < matrix1.GetLength(0); i++)
            {
                for (int j = 0; j < matrix1.GetLength(1); j++)
                {
                    result[i, j] = matrix1[i, j] + matrix2[i, j];
                }
            }
            return result;
        }

        static int[,] SubtractMatrices(int[,] matrix1, int[,] matrix2)
        {
            int[,] result = new int[matrix1.GetLength(0), matrix1.GetLength(1)];
            for (int i = 0; i < matrix1.GetLength(0); i++)
            {
                for (int j = 0; j < matrix1.GetLength(1); j++)
                {
                    result[i, j] = matrix1[i, j] - matrix2[i, j];
                }
            }
            return result;
        }

        static int[,] MultiplyMatrices(int[,] matrix1, int[,] matrix2)
        {
            int[,] result = new int[matrix1.GetLength(0), matrix1.GetLength(1)];
            for (int i = 0; i < matrix1.GetLength(0); i++)
            {
                for (int j = 0; j < matrix1.GetLength(1); j++)
                {
                    result[i, j] = 0;
                    for (int k = 0; k < matrix1.GetLength(1); k++)
                    {
                        result[i, j] += matrix1[i, k] * matrix2[k, j];
                    }
                }
            }
            return result;
        }

        static int[,] TransposeMatrix(int[,] matrix)
        {
            int[,] result = new int[matrix.GetLength(1), matrix.GetLength(0)];
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    result[j, i] = matrix[i, j];
                }
            }
            return result;
        }

        static double[,] InverseMatrix(int[,] matrix)
        {
            int n = matrix.GetLength(0);
            double[,] result = new double[n, n];
            double det = Determinant(matrix, n);
            if (det == 0)
            {
                return null;
            }

            double[,] adj = Adjoint(matrix);
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    result[i, j] = adj[i, j] / det;
                }
            }
            return result;
        }

        static double Determinant(int[,] matrix, int n)
        {
            double det = 0;
            if (n == 1)
            {
                return matrix[0, 0];
            }

            int[,] temp = new int[n, n];
            int sign = 1;

            for (int f = 0; f < n; f++)
            {
                GetCofactor(matrix, temp, 0, f, n);
                det += sign * matrix[0, f] * Determinant(temp, n - 1);
                sign = -sign;
            }

            return det;
        }

        static void GetCofactor(int[,] matrix, int[,] temp, int p, int q, int n)
        {
            int i = 0, j = 0;

            for (int row = 0; row < n; row++)
            {
                for (int col = 0; col < n; col++)
                {
                    if (row != p && col != q)
                    {
                        temp[i, j++] = matrix[row, col];
                        if (j == n - 1)
                        {
                            j = 0;
                            i++;
                        }
                    }
                }
            }
        }

        static double[,] Adjoint(int[,] matrix)
        {
            int n = matrix.GetLength(0);
            double[,] adj = new double[n, n];

            if (n == 1)
            {
                adj[0, 0] = 1;
                return adj;
            }

            int sign = 1;
            int[,] temp = new int[n, n];

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    GetCofactor(matrix, temp, i, j, n);
                    sign = ((i + j) % 2 == 0) ? 1 : -1;
                    adj[j, i] = (sign) * (Determinant(temp, n - 1));
                }
            }
            return adj;
        }
    }

    public class BTVNBuoi6_Slide25()
    {
        public static void Slide17EX()
        {
            // Đọc số nhóm sinh viên từ bàn phím
            int numGroups = GetValidInteger("Nhập số nhóm sinh viên: ");
            string[][] studentGroups = new string[numGroups][];

            // Đọc thông tin sinh viên cho từng nhóm
            for (int i = 0; i < numGroups; i++)
            {
                int numStudents = GetValidInteger($"Nhập số sinh viên trong nhóm {i + 1}: ");
                studentGroups[i] = new string[numStudents];
                for (int j = 0; j < numStudents; j++)
                {
                    Console.Write($"Nhập tên sinh viên {j + 1} của nhóm {i + 1}: ");
                    studentGroups[i][j] = Console.ReadLine();
                }
            }

            // Hiển thị thông tin sinh viên theo từng nhóm
            Console.WriteLine("\nDanh sách sinh viên theo từng nhóm:");
            for (int i = 0; i < numGroups; i++)
            {
                Console.WriteLine($"Nhóm {i + 1}:");
                for (int j = 0; j < studentGroups[i].Length; j++)
                {
                    Console.WriteLine($"  Sinh viên {j + 1}: {studentGroups[i][j]}");
                }
            }
        }

        static int GetValidInteger(string prompt)
        {
            int result;
            while (true)
            {
                Console.Write(prompt);
                string input = Console.ReadLine();
                if (int.TryParse(input, out result) && result > 0)
                {
                    return result;
                }
                Console.WriteLine("Giá trị nhập vào không hợp lệ. Vui lòng nhập lại.");
            }
        }
    }

    public class BTVNBuoi6_Slide23()
    {
        public static void Slide23EX_A()
        {
            // Nhập kích thước của ma trận vuông
            int n = GetValidInteger("Nhập kích thước của ma trận vuông (N x N): ");
            int[,] matrix = new int[n, n];

            // Nhập các phần tử của ma trận
            Console.WriteLine("Nhập các phần tử của ma trận:");
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    matrix[i, j] = GetValidInteger($"Nhập phần tử [{i + 1}, {j + 1}]: ");
                }
            }

            // Tính tổng các phần tử trên đường chéo chính
            int mainDiagonalSum = 0;
            for (int i = 0; i < n; i++)
            {
                mainDiagonalSum += matrix[i, i];
            }
            Console.WriteLine($"Tổng các phần tử trên đường chéo chính: {mainDiagonalSum}");

            // Tính tổng các phần tử trên đường chéo phụ
            int secondaryDiagonalSum = 0;
            for (int i = 0; i < n; i++)
            {
                secondaryDiagonalSum += matrix[i, n - 1 - i];
            }
            Console.WriteLine($"Tổng các phần tử trên đường chéo phụ: {secondaryDiagonalSum}");

            // Tính tổng các phần tử nằm phía trên đường chéo chính
            int aboveMainDiagonalSum = 0;
            for (int i = 0; i < n; i++)
            {
                for (int j = i + 1; j < n; j++)
                {
                    aboveMainDiagonalSum += matrix[i, j];
                }
            }
            Console.WriteLine($"Tổng các phần tử nằm phía trên đường chéo chính: {aboveMainDiagonalSum}");

            // Tính tổng các phần tử nằm phía dưới đường chéo chính
            int belowMainDiagonalSum = 0;
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < i; j++)
                {
                    belowMainDiagonalSum += matrix[i, j];
                }
            }
            Console.WriteLine($"Tổng các phần tử nằm phía dưới đường chéo chính: {belowMainDiagonalSum}");
        }
        public static void Slide23EX_B()
        {
            // Nhập kích thước của ma trận
            int n = GetValidInteger("Nhập số hàng của ma trận (N): ");
            int m = GetValidInteger("Nhập số cột của ma trận (M): ");
            int[,] matrix = new int[n, m];

            // Nhập các phần tử của ma trận
            Console.WriteLine("Nhập các phần tử của ma trận:");
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    matrix[i, j] = GetValidInteger($"Nhập phần tử [{i + 1}, {j + 1}]: ");
                }
            }

            // Sắp xếp ma trận
            SortMatrixByRowSums(matrix);
            SortMatrixByColumnSums(matrix);

            // Hiển thị ma trận sau khi sắp xếp
            Console.WriteLine("Ma trận sau khi sắp xếp:");
            PrintMatrix(matrix);
        }
        static int GetValidInteger(string prompt) // Hàm kiểm tra xem người dùng có nhập đúng dữ liệu không
        {
            int result;
            while (true)
            {
                Console.Write(prompt);
                string input = Console.ReadLine();
                if (int.TryParse(input, out result))
                {
                    return result;
                }
                Console.WriteLine("Giá trị nhập vào không hợp lệ. Vui lòng nhập lại.");
            }
        }
        static void SortMatrixByRowSums(int[,] matrix)
        {
            int rows = matrix.GetLength(0);
            int cols = matrix.GetLength(1);

            var rowSums = new int[rows];
            for (int i = 0; i < rows; i++)
            {
                rowSums[i] = 0;
                for (int j = 0; j < cols; j++)
                {
                    rowSums[i] += matrix[i, j];
                }
            }

            var rowOrder = rowSums
                .Select((sum, index) => new { sum, index })
                .OrderBy(x => x.sum)
                .Select(x => x.index)
                .ToArray();

            var sortedMatrix = new int[rows, cols];
            for (int i = 0; i < rows; i++)
            {
                int rowIndex = rowOrder[i];
                for (int j = 0; j < cols; j++)
                {
                    sortedMatrix[i, j] = matrix[rowIndex, j];
                }
            }

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    matrix[i, j] = sortedMatrix[i, j];
                }
            }
        }
        static void SortMatrixByColumnSums(int[,] matrix)
        {
            int rows = matrix.GetLength(0);
            int cols = matrix.GetLength(1);

            var colSums = new int[cols];
            for (int j = 0; j < cols; j++)
            {
                colSums[j] = 0;
                for (int i = 0; i < rows; i++)
                {
                    colSums[j] += matrix[i, j];
                }
            }

            var colOrder = colSums
                .Select((sum, index) => new { sum, index })
                .OrderBy(x => x.sum)
                .Select(x => x.index)
                .ToArray();

            var sortedMatrix = new int[rows, cols];
            for (int j = 0; j < cols; j++)
            {
                int colIndex = colOrder[j];
                for (int i = 0; i < rows; i++)
                {
                    sortedMatrix[i, j] = matrix[i, colIndex];
                }
            }

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    matrix[i, j] = sortedMatrix[i, j];
                }
            }
        }
        static void PrintMatrix(int[,] matrix)
        {
            int rows = matrix.GetLength(0);
            int cols = matrix.GetLength(1);

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    Console.Write(matrix[i, j] + "\t");
                }
                Console.WriteLine();
            }
        }
        
    }
}

