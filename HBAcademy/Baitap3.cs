﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace HBAcademy
{
    internal class Baitap3
    {
        
        public static void P1()
        {
            int x = 5;
            string s = "ABC";
            Console.WriteLine(x.GetType());
            Console.WriteLine(s.GetType());
            Console.WriteLine(Marshal.SizeOf(x));
        }

        public static void P2() 
        {
            Console.WriteLine("Nhập một ký tự:");
            char inputChar = Console.ReadKey().KeyChar;
            Console.WriteLine();

            if (char.IsLower(inputChar))
            {
                Console.WriteLine("Đây là chữ thường.");
            }
            else if (char.IsUpper(inputChar))
            {
                Console.WriteLine("Đây là chữ hoa.");
            }
            else if (char.IsDigit(inputChar))
            {
                Console.WriteLine("Đây là số.");
            }
            else
            {
                Console.WriteLine("Đây là ký tự đặc biệt.");
            }
        }
    }
}
